#include <iostream>
#include "RenderSystem.h"
using namespace std;


int main()
{
	RenderSystem* appRenderSystem = new RenderSystem();

	appRenderSystem->runMainLoop();

	return 0;
}