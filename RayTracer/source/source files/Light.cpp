#include "Light.h"
using namespace std;

vec3 Light::getLightDirection()
{
	return direction;
}

LightType Light::getLightType()
{
	return lightType;
}

vec4 Light::getLightColor()
{
	return color;
}

void Light::setLightDirection(vec3 newDirection)
{
	direction = newDirection;
}

void Light::setColor(vec4 newCol)
{
	color = newCol;
}

void Light::draw()
{
};

Light::Light()
{
	position = vec3(0.0);
	lightType = LightType::POINT;
	direction = vec3(0.0);
	color = vec4(1.0);
}

Light::Light(LightType type, vec3 pos, vec4 col)
{
	position = pos;
	lightType = type;
	direction = vec3(0.0);
	color = col;
}

Light::~Light()
{

}