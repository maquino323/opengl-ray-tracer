#include "Camera.h"
#include "glfw3.h"
using namespace std;

Camera::Camera()
{
	position = vec3(0.0);
	worldUp = vec3(0.0, 1.0, 0.0);
	up = vec3(0.0, 1.0, 1.0);
	direction = vec3(0.0);
	right = vec3(1.0, 0.0, 0.0);
	front = vec3(0.0, 0.0, -1.0);
	viewMatrix = mat4(1.0);
}

Camera::Camera(vec3 pos)
{
	position = pos;
	worldUp = vec3(0.0, 1.0, 0.0);

	vec3 target = vec3(0.0);
	direction = normalize(position - target);
	right = normalize(cross(worldUp, direction));
	up = cross(direction, right);
	front = vec3(0.0, 0.0, -1.0);
	getViewMatrix();
}

Camera::~Camera()
{
}

void Camera::processKeyboardInput(int key, float dt)
{
	float velocity = speed * dt;

	// LEFT RIGHT
	if (key == GLFW_KEY_D)
		position += velocity * right;

	if (key == GLFW_KEY_A)
		position -= velocity * right;

	// FORWARD BACKWARD
	if (key == GLFW_KEY_W)
		position += velocity * front;

	if (key == GLFW_KEY_S)
		position -= velocity * front;

	// UP DOWN
	if (key == GLFW_KEY_E)
		position += velocity * worldUp;

	if (key == GLFW_KEY_Q)
		position -= velocity * worldUp;
	
}


mat4 Camera::getViewMatrix()
{
	viewMatrix = lookAt(position, position + front, up);
	return viewMatrix;
}

void Camera::updateCameraDirection(vec3 newDir)
{
	front = newDir;
	right = normalize(cross(front, worldUp));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
	up = normalize(cross(right, front));
}
