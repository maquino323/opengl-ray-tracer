#include "Shader.h"
#include "glew.h"
using namespace std;

Shader::Shader(const char* vShaderPath, const char* fShaderPath)
{
	string vCode, fCode; // 
	ifstream vsFile, fsFile;
	vsFile.exceptions(ifstream::failbit | ifstream::badbit);
	fsFile.exceptions(ifstream::failbit | ifstream::badbit);

	/*
	* 1. open shader files
	* 2. read file buffer's content into streams
	* 3. close files
	* 4. convert streams into strings
	* 
	* catch if fails
	*/
	try
	{
		vsFile.open(vShaderPath);
		fsFile.open(fShaderPath);
		stringstream vsStream, fsStream;
		vsStream << vsFile.rdbuf();
		fsStream << fsFile.rdbuf();
		vsFile.close();
		fsFile.close();
		vCode = vsStream.str();
		fCode = fsStream.str();
	}

	catch (ifstream::failure err)
	{
		cerr << "Shader File Not Successfully Read!\n";
	}

	const char* vShaderCode = vCode.c_str();
	const char* fShaderCode = fCode.c_str();

	// compile and link shaders
	unsigned int vert, frag;
	int success;
	char infoLog[32];

	vert = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vert, 1, &vShaderCode, NULL);
	glCompileShader(vert);
	glGetShaderiv(vert, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(vert, 512, NULL, infoLog);
		cerr << "VERTEX SHADER COMPILATION FAILED\n" << infoLog << endl;
	}

	frag = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(frag, 1, &fShaderCode, NULL);
	glCompileShader(frag);
	glGetShaderiv(frag, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(frag, 512, NULL, infoLog);
		cerr << "FRAGMENT SHADER COMPILATION FAILED\n" << infoLog << endl;
	}

	shaderID = glCreateProgram();
	glAttachShader(shaderID, vert);
	glAttachShader(shaderID, frag);
	glLinkProgram(shaderID);

	glGetProgramiv(shaderID, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderID, 512, NULL, infoLog);
		cerr << "SHADER PROGRAM LINKING FAILED\n" << infoLog << endl;
	}

	glDeleteShader(vert);
	glDeleteShader(frag);
}


Shader::Shader(const char* cShaderPath)
{
	string cCode; // 
	ifstream csFile;
	csFile.exceptions(ifstream::failbit | ifstream::badbit);

	/*
	* 1. open shader files
	* 2. read file buffer's content into streams
	* 3. close files
	* 4. convert streams into strings
	*
	* catch if fails
	*/
	try
	{
		csFile.open(cShaderPath);
		stringstream csStream;
		csStream << csFile.rdbuf();
		csFile.close();
		cCode = csStream.str();
	}

	catch (ifstream::failure err)
	{
		cerr << "Shader File Not Successfully Read!\n";
	}

	const char* cShaderCode = cCode.c_str();

	// compile and link shaders
	unsigned int comp;
	int success;
	char infoLog[32];

	comp = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(comp, 1, &cShaderCode, NULL);
	glCompileShader(comp);
	glGetShaderiv(comp, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(comp, 512, NULL, infoLog);
		cerr << "VERTEX SHADER COMPILATION FAILED\n" << infoLog << endl;
	}

	shaderID = glCreateProgram();
	glAttachShader(shaderID, comp);
	glLinkProgram(shaderID);

	glGetProgramiv(shaderID, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderID, 512, NULL, infoLog);
		cerr << "SHADER PROGRAM LINKING FAILED\n" << infoLog << endl;
	}

	glDeleteShader(comp);
}


void Shader::useShaderProgram()
{
	glUseProgram(shaderID);
}

unsigned int Shader::getShaderID()
{
	return shaderID;
}


void Shader::setBool(const string& varName, bool value) const
{
	glUniform1i(glGetUniformLocation(shaderID, varName.c_str()), (int)value);
}

void Shader::setInt(const string& varName, int value) const
{
	glUniform1i(glGetUniformLocation(shaderID, varName.c_str()), value);
}

void Shader::setFloat(const string& varName, float value) const
{
	glUniform1f(glGetUniformLocation(shaderID, varName.c_str()), value);
}

void Shader::setVec3(const string& varName, vec3 value) const
{
	glUniform3f(glGetUniformLocation(shaderID, varName.c_str()), value.x, value.y, value.z);
}

void Shader::setVec4(const string& varName, vec4 value) const
{
	glUniform4f(glGetUniformLocation(shaderID, varName.c_str()), value.r, value.g, value.b, value.a);
}

void Shader::setMat4(const string& varName, mat4 value) const
{
	glUniformMatrix4fv(glGetUniformLocation(shaderID, varName.c_str()), 1, GL_FALSE, &value[0][0]);
}

// default constructor, destructor
Shader::Shader()
{
	shaderID = 0;
}


Shader::~Shader()
{

}