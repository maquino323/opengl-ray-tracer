#include "SceneObject.h"
#include "glew.h"
#include <vector>
using namespace std;

// Abstract Scene Object class
#pragma region SCENEOBJECT

void SceneObject::setPosition(vec3 newPos)
{
	position = newPos;
}

void SceneObject::setRotation(vec3 newRot)
{
	eulerAngles = newRot;
	quatRotation = quat(eulerAngles); // convert into quaternion rotation
}

void SceneObject::setRotation(quat newRot)
{
	quatRotation = newRot;
}

vec3 SceneObject::getPosition()
{
	return position;
}

vec3 SceneObject::getEulerAngles()
{
	return eulerAngles;
}

quat SceneObject::getQuatRotation()
{
	return quatRotation;
}



// default constructor, destructor
SceneObject::SceneObject()
{
	position = vec3(0.0);
	eulerAngles = vec3(0.0);
	quatRotation = quat(0, 0, 0, 1);
	vao = 0;
	vbo = 0;
}

SceneObject::~SceneObject()
{

}
#pragma endregion


// Cube Object class
#pragma region CUBE
Cube::Cube()
{
}

Cube::Cube(vec3 pos)
{
    position = pos;
}

Cube::~Cube()
{

}


void Cube::createVertexBufferObjects()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // vertices
    glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	
    // normals
    glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	
    glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}

void Cube::draw()
{
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

#pragma endregion

// Sphere Object class
#pragma region SPHERE
Sphere::Sphere()
{
	radius = 1;
    indexCount = 0;
    createVertexBufferObjects();
}

Sphere::~Sphere()
{

}

void Sphere::createVertexBufferObjects()
{
     glGenVertexArrays(1, &vao);

     unsigned int vbo, ebo;
     glGenBuffers(1, &vbo);
     glGenBuffers(1, &ebo);

     vector<vec3> positions;
     vector<vec2> uv;
     vector<vec3> normals;
     vector<unsigned int> indices;

     const unsigned int X_SEGMENTS = 64;
     const unsigned int Y_SEGMENTS = 64;
     const float PI = 3.14159265359;
     for (unsigned int y = 0; y <= Y_SEGMENTS; ++y)
     {
         for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
         {
             float xSegment = (float)x / (float)X_SEGMENTS;
             float ySegment = (float)y / (float)Y_SEGMENTS;
             float xPos = cos(xSegment * 2.0f * PI) * sin(ySegment * PI);
             float yPos = cos(ySegment * PI);
             float zPos = sin(xSegment * 2.0f * PI) * sin(ySegment * PI);

             positions.push_back(vec3(xPos, yPos, zPos));
             uv.push_back(vec2(xSegment, ySegment));
             normals.push_back(vec3(xPos, yPos, zPos));
         }
     }

     bool oddRow = false;
     for (unsigned int y = 0; y < Y_SEGMENTS; ++y)
     {
         if (!oddRow) // even rows: y == 0, y == 2; and so on
         {
             for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
             {
                 indices.push_back(y * (X_SEGMENTS + 1) + x);
                 indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
             }
         }
         else
         {
             for (int x = X_SEGMENTS; x >= 0; --x)
             {
                 indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
                 indices.push_back(y * (X_SEGMENTS + 1) + x);
             }
         }
         oddRow = !oddRow;
     }
     indexCount = indices.size();

     vector<float> data;
     for (size_t i = 0; i < positions.size(); ++i)
     {
         data.push_back(positions[i].x);
         data.push_back(positions[i].y);
         data.push_back(positions[i].z);
         if (uv.size() > 0)
         {
             data.push_back(uv[i].x);
             data.push_back(uv[i].y);
         }
         if (normals.size() > 0)
         {
             data.push_back(normals[i].x);
             data.push_back(normals[i].y);
             data.push_back(normals[i].z);
         }
     }
     glBindVertexArray(vao);
     glBindBuffer(GL_ARRAY_BUFFER, vbo);
     glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);
     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
     glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

     float stride = (3 + 2 + 3) * sizeof(float);

     // vertices
     glEnableVertexAttribArray(0);
     glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)0);

     // normals
     glEnableVertexAttribArray(1);
     glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)(3 * sizeof(float)));

     // uvs
     glEnableVertexAttribArray(2);
     glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride, (void*)(5 * sizeof(float)));

     glBindBuffer(GL_ARRAY_BUFFER, 0);
     glBindVertexArray(0);

}

void Sphere::draw()
{
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLE_STRIP, indexCount, GL_UNSIGNED_INT, 0);
}
#pragma endregion