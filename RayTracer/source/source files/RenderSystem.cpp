#include "RenderSystem.h"
#include <iostream>

using namespace std;

// define shader file paths
#define VERT "././source/shaders/vert/"
#define FRAG "././source/shaders/frag/"
#define COMP "././source/shaders/comp/"


void framebuffer_size_callback(GLFWwindow* win, int width, int height)
{
	glViewport(0, 0, width, height);
}

void cursor_callback(GLFWwindow* win, double xPos, double yPos)
{
	if (firstMouse) // initially set to true
	{
		lastX = xPos;
		lastY = yPos;
		firstMouse = false;
	}

	float xOffset = xPos - lastX;
	float yOffset = lastY - yPos;
	lastX = xPos;
	lastY = yPos;

	float sensitivity = 0.1f;
	xOffset *= sensitivity;
	yOffset *= sensitivity;

	cameraYaw += xOffset;
	cameraPitch += yOffset;

	if (cameraPitch > 89.0f)
		cameraPitch = 89.0f;

	else if (cameraPitch < -89.0f)
		cameraPitch = -89.0f;

	vec3 dir;
	dir.x = cos(radians(cameraYaw)) * cos(radians(cameraPitch));
	dir.y = sin(radians(cameraPitch));
	dir.z = sin(radians(cameraYaw)) * cos(radians(cameraPitch));

	cameraDirection = normalize(dir);
}

void scroll_callback(GLFWwindow* window, double xOff, double yOff)
{
	FOV -= (float)yOff;
	if (FOV < 1.0f)
		FOV = 1.0f;
	if (FOV > 45.0f)
		FOV = 45.0f;
}


void RenderSystem::initializeOpenGL()
{
	// init everything

	if (!glfwInit())
	{
		cerr << "Failed to initialize GLFW\n";
		exit(-1);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	screenWidth = 1280;
	screenHeight = 720;

	appWindow = glfwCreateWindow(screenWidth, screenHeight, "OpenGL Ray Tracer", NULL, NULL);
	if (!appWindow)
	{
		cerr << "Failed to create GLFW window\n";
		glfwTerminate();
		exit(-1);
	}

	glfwMakeContextCurrent(appWindow);


	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		cerr << "Failed to initialize GLEW\n";
		exit(-1);
	}

	glfwSetInputMode(appWindow, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(appWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glClearColor(0.0, 0.0, 0.0, 1.0);

	glViewport(0, 0, screenWidth, screenHeight);
	glfwSetFramebufferSizeCallback(appWindow, framebuffer_size_callback);
	glfwSetCursorPosCallback(appWindow, cursor_callback);
	glfwSetScrollCallback(appWindow, scroll_callback);

	// set up graphics settings
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

}

void RenderSystem::setUpScene()
{
	sceneCamera = Camera(vec3(0.0, 0.0, 5.0));
	objectShader = Shader(VERT"SceneObject_vertex_shader.glsl", FRAG"SceneObject_fragment_shader.glsl");

	// create objects
	cube = Cube(vec3(0.0, 0.0, 0.0));
	cube.createVertexBufferObjects();

	// create uniform matrices
	view = sceneCamera.getViewMatrix();
	projection = perspective(radians(45.0f), float(screenWidth / screenHeight), 0.1f, 1000.0f);

	sceneLight = Light(LightType::POINT, vec3(1.0, 0.0, 1.0), vec4(1.0));
}

void RenderSystem::runMainLoop()
{
	// only exit when user hits ESC key
	while (glfwWindowShouldClose(appWindow) == 0)
	{
		/*
		* Loop Steps:
		*	1. Check User Input
		*	2. Update All Necessary Variables
		*	3. Render Scene
		*/

		currentTime = glfwGetTime();
		deltaTime = currentTime - lastTime;
		lastTime = currentTime;
		appTime += deltaTime;

		cout << "\rApp Time: " << appTime;

		checkInput();
		update();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// set shader uniforms
		objectShader.useShaderProgram();
		objectShader.setMat4("view", view);
		objectShader.setMat4("projection", projection);
		objectShader.setVec3("lightPosition", sceneLight.getPosition());
		objectShader.setVec4("lightColor", sceneLight.getLightColor());

		model = mat4(1.0);
		scale(model, vec3(0.1, 0.1, 0.1));						// scale cube down
		rotate(model, radians(appTime), vec3(0.0, 1.0, 0.0));	// rotate along y axis
		//translate(model, cube.getPosition());					// translate to position
		objectShader.setMat4("model", model);
		cube.draw();

		glfwSwapBuffers(appWindow);
		glfwPollEvents();

	};


	cout << endl << endl;
	glfwTerminate();
}

void RenderSystem::checkInput()
{
	// close program
	if (glfwGetKey(appWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(appWindow, 1);

	// move camera
	if (glfwGetKey(appWindow, GLFW_KEY_W) == GLFW_PRESS)
		sceneCamera.processKeyboardInput(GLFW_KEY_W, deltaTime);
	

	if (glfwGetKey(appWindow, GLFW_KEY_S) == GLFW_PRESS)
		sceneCamera.processKeyboardInput(GLFW_KEY_S, deltaTime);
	

	if (glfwGetKey(appWindow, GLFW_KEY_A) == GLFW_PRESS)
		sceneCamera.processKeyboardInput(GLFW_KEY_A, deltaTime);
	

	if (glfwGetKey(appWindow, GLFW_KEY_D) == GLFW_PRESS)
		sceneCamera.processKeyboardInput(GLFW_KEY_D, deltaTime);

	if (glfwGetKey(appWindow, GLFW_KEY_Q) == GLFW_PRESS)
		sceneCamera.processKeyboardInput(GLFW_KEY_Q, deltaTime);

	if (glfwGetKey(appWindow, GLFW_KEY_E) == GLFW_PRESS)
		sceneCamera.processKeyboardInput(GLFW_KEY_E, deltaTime);

}


void RenderSystem::update()
{
	view = sceneCamera.getViewMatrix();
	sceneCamera.updateCameraDirection(cameraDirection);

	sceneLight.setPosition(vec3(sin(appTime), 2.0, cos(appTime)));
}

void RenderSystem::setScreenResolution(int width, int height)
{
	screenWidth = width;
	screenHeight = height;
}

// default constructor, destructor
RenderSystem::RenderSystem()
{
	appWindow = NULL;
	initializeOpenGL();
	setUpScene();
}

RenderSystem::~RenderSystem()
{
}
