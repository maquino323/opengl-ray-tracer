#version 460 core

in vec3 vPos;
in vec3 vNormal;

uniform vec3 lightPosition;
uniform vec4 lightCol;
out vec4 fragColor;

void main()
{
	vec3 normal = normalize(vNormal);
	vec3 dir = normalize(lightPosition - vPos);
	float diff = max(dot(normal, dir), 0.0);

	vec3 ambient = vec3(0.1, 0.1, 0.1);
	vec3 diffuse = vec3(lightCol) * diff * 5; // intensity of the light
	vec3 albedo = vec3(1.0, 0.0, 0.0);


	fragColor = vec4((ambient + diffuse) * albedo, 1.0);
}