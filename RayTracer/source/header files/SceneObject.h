#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "glm.hpp"
#include "gtc/quaternion.hpp"
using namespace glm;
using namespace std;

class SceneObject
{
public:
	SceneObject();
	~SceneObject();

	// Purpose: set an object's position in 3D space
	void setPosition(vec3 newPos);

	// Purpose: set an object's orientation using Euler angles
	void setRotation(vec3 newRot);

	// Purpose: set an object's orientation using quaternions
	void setRotation(quat newRot);

	// Purpose: set an object's orientation using axis-angle quaternion rotation
	void setRotation(float rotationAngle, vec3 rotationAxis);

	// Purpose: get an object's position in 3D space
	vec3 getPosition();

	// Purpose: get an object's orientaion in Euler angles
	vec3 getEulerAngles();

	// Purpose: get an object's orientation in quaternions
	quat getQuatRotation();

    virtual void draw() = 0;
	
protected:
	vec3 position;
	vec3 eulerAngles;
	quat quatRotation;
	unsigned int vao, vbo;
};


class Cube : public SceneObject
{
public:
    Cube();
    Cube(vec3 pos);
    ~Cube();

    void createVertexBufferObjects();

    // Purpose: Draw the current object using the object's VAO and VBO
    void draw();

private:
    float vertices[216] =
    {
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
    };
};

class Sphere : public SceneObject
{
public:
    Sphere();
    ~Sphere();

    void createVertexBufferObjects();

    // Purpose: Draw the current object using the object's VAO and VBO
    void draw();

private:
    float radius;
    int indexCount;
};

#endif //!SCENEOBJECT_H