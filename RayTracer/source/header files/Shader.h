#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include "matrix.hpp"
using namespace std;
using namespace glm;


class Shader
{
public:
	Shader();
	~Shader();

	// Purpose: Create shader program for objects using a vertex/fragment shader pipeline
	Shader(const char* vShaderPath, const char* fShaderPath);

	// Purpose: Create a compute shader program to handle calculations in parallel on GPU
	Shader(const char* cShaderPath);

	// Purpose: Force OpenGL to use this shader to render current objects
	void useShaderProgram();
	
	// Purpose: Grab shader ID
	unsigned int getShaderID();


	// Purpose: Set uniform bool within shader
	void setBool(const string &varName, bool value) const;

	// Purpose: Set uniform int within shader
	void setInt(const string &varName, int value) const;

	// Purpose: Set uniform float within shader
	void setFloat(const string &varName, float value) const;

	// Purpose: Set uniform matrix within shader
	void setMat4(const string& varName, mat4 value) const;
	
	// Purpose: Set uniform vec3 within shader
	void setVec3(const string& varName, vec3 value) const;
	
	// Purpose: Set uniform vec3 within shader
	void setVec4(const string& varName, vec4 value) const;

private:
	unsigned int shaderID;

};


#endif //!SHADER_H