#ifndef LIGHT_H
#define LIGHT_H

#include "SceneObject.h"
using namespace std;

enum class LightType
{
	POINT,
	DIRECTIONAL,
	SPOTLIGHT,
	AREA
};

class Light : public SceneObject
{
public:
	Light();
	Light(LightType type, vec3 pos, vec4 col);
	~Light();

	LightType getLightType();

	void setLightDirection(vec3 newDirection);
	void setColor(vec4 newCol);

	vec4 getLightColor();
	vec3 getLightDirection();

	void draw();

private:
	vec3 position;
	vec3 direction;
	LightType lightType;
	vec4 color;
};


#endif //!LIGHT_H