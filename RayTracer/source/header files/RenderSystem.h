#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include "glew.h"
#include "glfw3.h"
#include "glm.hpp"

#include <vector>
#include "SceneObject.h"
#include "Shader.h"
#include "Camera.h"
#include "Light.h"

using namespace std;

void framebuffer_size_callback(GLFWwindow* win, int width, int height);
void cursor_callback(GLFWwindow* win, double xPos, double yPos);
void scroll_callback(GLFWwindow* win, double xOff, double yOff);

class RenderSystem
{
public:
	RenderSystem();
	~RenderSystem();


	void runMainLoop();
	void checkInput();
	void update();
	void setUpScene();

	void setScreenResolution(int width, int height);

private:
	GLFWwindow* appWindow;

	Shader objectShader;
	mat4 model, view, projection;
	Camera sceneCamera;
	float deltaTime, currentTime, lastTime;

	float appTime;

	Light sceneLight;

	int screenWidth, screenHeight;

	Cube cube;

	void initializeOpenGL();
};


#endif //!RENDERSYSTEM_H