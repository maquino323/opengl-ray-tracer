#ifndef CAMERA_H
#define CAMERA_H

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
using namespace glm;
using namespace std;

static float lastX = 514;
static float lastY = 384;
static float cameraYaw = -90.0f;
static float cameraPitch = 0.0f;
static float FOV = 45.0f;
static bool firstMouse = true;
static vec3 cameraDirection = vec3(0.0);

class Camera
{
public:
	Camera();
	Camera(vec3 pos);
	~Camera();

	mat4 getViewMatrix();
	void updateCameraDirection(vec3 newDir);

	void processKeyboardInput(int key, float dt);



private:
	vec3 position;
	vec3 worldUp;
	vec3 direction;
	vec3 up;
	vec3 right;
	vec3 front;

	float speed = 1.0f;

	mat4 viewMatrix;
};


#endif //!CAMERA_H